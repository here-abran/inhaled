﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_move_script : MonoBehaviour
{
    public int pleyerSpeed = 10;
    public int playerJumpPower = 1800;
    private float moveX;
    public bool isGrounded;

    public AudioClip jump;
    AudioSource audio;
    
    void Start()
    {
        audio = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        PlayerRaycast();
        PlayerMove();
    }

    void PlayerMove() {
        //Controls
        moveX = Input.GetAxis("Horizontal");
        if (Input.GetButtonDown("Jump") && isGrounded == true)
        {
            Jump();
            GetComponent<Animator>().SetBool("IsJump", true);
        } else {
            GetComponent<Animator>().SetBool("IsJump", false);
        }

        //Animations
        if(moveX != 0)
        {
            GetComponent<Animator>().SetBool("IsRunning", true);
        } else {
            GetComponent<Animator>().SetBool("IsRunning", false);
        }

        //Player Direction
        if (moveX < 0.0f && !GetComponent<Animator>().GetBool("IsDie"))
        {
            GetComponent<SpriteRenderer>().flipX = true;
        }
        else if (moveX > 0.0f)
        {
            GetComponent<SpriteRenderer>().flipX = false;
        }

        //Physics
        if(!GetComponent<Animator>().GetBool("IsDie"))
        {
            gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2 (moveX * pleyerSpeed, gameObject.GetComponent<Rigidbody2D>().velocity.y);
        }
    }

    void Jump() {
        //Jumping Code
        isGrounded = false;
        GetComponent<Rigidbody2D>().AddForce(Vector2.up * playerJumpPower);
        audio.PlayOneShot(jump, 0.7f);
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        // Debug.Log("Player has Collided with " + col.collider.name);
        if(col.gameObject.tag == "Ground")
        {
            isGrounded = true;
        }
    }

    void PlayerRaycast()
    {
        RaycastHit2D hit = Physics2D.Raycast(this.gameObject.transform.position, Vector2.down);
        if (hit.collider.tag == "enemy")
        {
            hit.collider.GetComponent<Animator>().SetBool("IsDie", true);
            Player_health.health = 0;
        }
    }
}
