﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player_health : MonoBehaviour
{
    public static int health = 100;

    // Update is called once per frame
    void Update()
    {
        if (gameObject.transform.position.y < -7 || health <= 0)
        {
            Die();
        }
    }

    void Die()
    {
        SceneManager.LoadScene("GREEN-ZONE");
    }
}
