﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Player_Score : MonoBehaviour
{
    private float timeLeft = 120;
    public int playerScore;
    public Text timeLeftText;
    public Text playerScoreText;
    public Text playerHighScore;

    void Start()
    {
        DataManagement.dataManagement.LoadData();
    }

    void Update()
    {
        timeLeft -= Time.deltaTime;
        timeLeftText.text = "Time Left : " +  timeLeft;
        playerScoreText.text = "Score : " + (playerScore + DataManagement.dataManagement.playerScore);
        playerHighScore.text = "High Score : " + DataManagement.dataManagement.highScore;
        if (timeLeft < 0.1f)
        {
            SceneManager.LoadScene("GREEN-ZONE");
        }
    }

    void OnTriggerEnter2D(Collider2D trig)
    {
        if(trig.transform.tag == "Finish")
        {
            CountScore();
        }

        if(trig.transform.tag == "coin")
        {
            playerScore += 10;
            Destroy(trig.gameObject);
        }
    }

    void CountScore() 
    {
        playerScore = playerScore + (int)(timeLeft * 10);
        DataManagement.dataManagement.highScore = playerScore + (int)(timeLeft * 10);
        DataManagement.dataManagement.SaveData();
    }
}
