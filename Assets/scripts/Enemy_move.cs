﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_move : MonoBehaviour
{
    public int EnemySpeed;
    public int XMoveDirection;
    private float moveX;
    private Transform player;
    private float Distance;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
    }

    // Update is called once per frame
    void Update()
    {
        RaycastEnemy();
        moveX = gameObject.transform.position.x;
        Distance = Mathf.Abs(transform.position.x - player.position.x);
        if(Distance <= 8 && Distance > 2)
        {
            Running();
            GetComponent<Animator>().SetBool("IsAttack", false);
        }
        else if (Distance <= 2)
        {
            Running();
            GetComponent<Animator>().SetBool("IsAttack", true);
        }
        else {
            GetComponent<Animator>().SetBool("IsRunning", false);
            GetComponent<Animator>().SetBool("IsAttack", false);
        }
    }

    void Running()
    {
        if(player.position.x > transform.position.x)
        {
            XMoveDirection = 1;
            GetComponent<SpriteRenderer>().flipX = false;
        }
        else 
        {
            XMoveDirection = -1;
            GetComponent<SpriteRenderer>().flipX = true;
        }

        gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(XMoveDirection, 0) * EnemySpeed;
        if(moveX != 0)
        {
            GetComponent<Animator>().SetBool("IsRunning", true);
        } else {
            GetComponent<Animator>().SetBool("IsRunning", false);
        }
    }

    void RaycastEnemy()
    {
        RaycastHit2D hit = Physics2D.Raycast(transform.position, new Vector2(XMoveDirection, 0));
        if(hit.collider.tag == "Player")
        {
            hit.collider.GetComponent<Animator>().SetBool("IsDie", true);
        }
    }
}
